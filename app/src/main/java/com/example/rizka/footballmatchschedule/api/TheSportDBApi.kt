package com.example.rizka.footballmatchschedule.api

import android.net.Uri
import com.example.rizka.footballmatchschedule.BuildConfig

object TheSportDBApi {

    fun getTeams(idTeam: String?): String {
        return "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/lookupteam.php?id=" + idTeam
    }

    fun getNextMatch(league: String?): String {
        return "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/eventsnextleague.php?id=" + league
    }

    fun getPreviousMatch(leagueId: String?): String {
        return "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/eventspastleague.php?id=" + leagueId
    }

    fun getDetailMatch(eventId: String?): String {
        return "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/lookupevent.php?id=" + eventId
    }

}