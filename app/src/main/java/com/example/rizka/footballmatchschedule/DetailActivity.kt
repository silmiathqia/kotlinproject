package com.example.rizka.footballmatchschedule

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.widget.SwipeRefreshLayout
import android.widget.ProgressBar
import com.example.rizka.footballmatchschedule.api.ApiRepository
import com.example.rizka.footballmatchschedule.api.TheSportDBApi
import com.example.rizka.footballmatchschedule.main.MainView
import com.example.rizka.footballmatchschedule.model.EventsItem
import com.example.rizka.footballmatchschedule.model.Team
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.match_list.*

class DetailActivity : AppCompatActivity(), MainView {

    var idEvent: String = ""
    var idAway: String = ""
    var idHome: String = ""
    var nameHome: String = ""
    var nameAway: String = ""

    private lateinit var progresBar: ProgressBar
    private lateinit var presenter: DetailPresenter
    private lateinit var events: EventsItem

    companion object {
        const val ID_EVENTS = "id_events"
        const val ID_AWAY = "id_Away"
        const val ID_HOME = "id_Home"
        const val HOME_NAME = "home_name"
        const val AWAY_NAME = "away_home"
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)


        val intent = intent
        idEvent = intent.getStringExtra(ID_EVENTS)
        idAway = intent.getStringExtra(ID_AWAY)
        idHome = intent.getStringExtra(ID_HOME)
        nameAway = intent.getStringExtra(AWAY_NAME)
        nameHome = intent.getStringExtra(HOME_NAME)

        val request = ApiRepository()
        val gson = Gson()
        presenter = DetailPresenter(this, request, gson)
        presenter.getLookUpEvents(idEvent)

        println(TheSportDBApi.getDetailMatch(idEvent))

    }

    override fun hideLoading() {
        progresBar.invisible()
    }

    override fun showEventList(data: List<EventsItem>) {
        events = data[0]
        teamName1.text = nameHome
        teamName2.text = nameAway

        ScoreTeam1.text = events.strHomeGoalDetails?.replace(";", "\n")
        ScoreTeam1.text = events.strAwayGoalsDetails?.replace(";", "\n")

        if (events.intHomeScore != null) ScoreTeam1.text = events.intHomeScore.toString()
        if (events.intAwayScore != null) ScoreTeam1.text = events.intAwayScore.toString()

        Defense1.text = events.strHomeLineupDefense?.replace(";", "\n")
        Defense2.text = events.strAwayLineupDefense?.replace(";", "\n")

        Midfield1.text = events.strHomeLineupMidfield
        Midfield2.text = events.strAwayLineupMidfield

        Subtitutes1.text = events.strAwayLineupSubtitutes
        Subtitutes2.text = events.strHomeLineupSubtitutes

        Forward1.text = events.strAwayLineupForward
        Forward2.text = events.strHomeLineupForward


    }

    override fun showLoading() {
        progresBar.visible()

    }

    override fun showTeamList(data: List<Team>?) {

    }
}

