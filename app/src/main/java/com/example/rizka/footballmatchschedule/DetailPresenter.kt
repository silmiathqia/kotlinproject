package com.example.rizka.footballmatchschedule

import com.example.rizka.footballmatchschedule.api.ApiRepository
import com.example.rizka.footballmatchschedule.api.TheSportDBApi
import com.example.rizka.footballmatchschedule.main.MainView
import com.example.rizka.footballmatchschedule.model.DetailEventResponse
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class DetailPresenter (private val view: MainView,
                       private val apiRepository: ApiRepository,
                       private val gson: Gson) {

    fun getLookUpEvents(eventId: String?){
        view.showLoading()
        doAsync {
            val datas = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getDetailMatch(eventId)),
                DetailEventResponse::class.java
            )
            uiThread {
                view.showEventList(datas.events)
                view.hideLoading()
            }
        }
    }

}