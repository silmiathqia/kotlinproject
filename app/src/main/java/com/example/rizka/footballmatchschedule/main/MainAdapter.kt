package com.example.rizka.footballmatchschedule.main

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.rizka.footballmatchschedule.DetailActivity
import com.example.rizka.footballmatchschedule.R
import com.example.rizka.footballmatchschedule.model.EventsItem
import org.jetbrains.anko.startActivity
import java.text.SimpleDateFormat
import java.util.*

class MainAdapter(private val context: Context?, private val events: List<EventsItem>)
    : RecyclerView.Adapter<MainAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MainHolder(LayoutInflater.from(context).inflate(R.layout.match_list, parent, false))


    override fun getItemCount(): Int = events.size

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        holder.bindItem(events [position])
        holder.itemView.setOnClickListener {
            context?.startActivity<DetailActivity>(DetailActivity.ID_EVENTS to events[position].idEvent,
                DetailActivity.ID_HOME to events[position].idHomeTeam,
                DetailActivity.ID_AWAY to events[position].idAwayTeam,
                DetailActivity.AWAY_NAME to events[position].strAwayTeam,
                DetailActivity.HOME_NAME to events[position].strHomeTeam)
        }

    }

    class MainHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tanggal = view.findViewById<TextView>(R.id.date)
        private val strAway = view.findViewById<TextView>(R.id.teamName1)
        private val awayScore = view.findViewById<TextView>(R.id.ScoreTeam1)
        private val strHome = view.findViewById<TextView>(R.id.teamName2)
        private val homeScore = view.findViewById<TextView>(R.id.ScoreTeam2)

        @RequiresApi(Build.VERSION_CODES.O)
        fun bindItem (events: EventsItem){
            val formatDate = SimpleDateFormat("yyy-MM-dd", Locale.getDefault())
            val date = formatDate.parse(events.dateEvent)
            val dateText = SimpleDateFormat("EEEE, dd-MM-yyyy", Locale.getDefault())
                .format(date).toString()
            tanggal.text = dateText
            strAway.text = events.strAwayTeam
            strHome.text = events.strHomeTeam
            homeScore.text = events.intHomeScore
            awayScore.text = events.intAwayScore

        }
    }
}
