package com.example.rizka.footballmatchschedule.main

import com.example.rizka.footballmatchschedule.api.ApiRepository
import com.example.rizka.footballmatchschedule.api.TheSportDBApi
import com.example.rizka.footballmatchschedule.model.DetailEventResponse
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainPresenter(private val view: MainView,
                    private val apiRepository: ApiRepository,
                    private val gson: Gson) {

    fun getEventNext(leagueId: String?) {
        view.showLoading()
        doAsync {
            val datas = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getNextMatch(leagueId)),
                DetailEventResponse::class.java
            )
            uiThread {
                view.hideLoading()
                view.showEventList(datas.events)
            }
        }
    }

    fun getPastEvent(leagueId: String?) {
        view.showLoading()
        doAsync {
            val datas = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getPreviousMatch(leagueId)),
                DetailEventResponse::class.java
            )
            uiThread {
                view.hideLoading()
                view.showEventList(datas.events)
            }
        }
    }

}